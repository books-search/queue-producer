package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/streadway/amqp"
)

func info(w http.ResponseWriter, r *http.Request) {
	log.Println("Endpoint Hit: info")

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	info := struct {
		Version   string `json:"version"`
		Commit    string `json:"commit"`
		BuildTime string `json:"buildTime"`
	}{
		Version:   version,
		Commit:    commit,
		BuildTime: buildTime,
	}

	err := json.NewEncoder(w).Encode(info)

	if err != nil {
		log.Printf("Info handler, error during encode: %v", err)
		http.Error(w, "Something went wrong", http.StatusServiceUnavailable)
	}
}

func pong(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong!")
	log.Println("Endpoint Hit: ping")
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func putInQueue(w http.ResponseWriter, r *http.Request) {
	// get the body of our POST request
	// return the string response containing the request body
	log.Println("Endpoint Hit: put")
	reqBody, err := ioutil.ReadAll(r.Body)

	if err != nil {
		log.Fatal(err)
	}

	var BOOKS_QUEUE_SERVICE_HOST string
	if BOOKS_QUEUE_SERVICE_HOST = os.Getenv("BOOKS_QUEUE_SERVICE_HOST"); BOOKS_QUEUE_SERVICE_HOST == "" {
		BOOKS_QUEUE_SERVICE_HOST = "localhost"
	}

	var BOOKS_QUEUE_SERVICE_PORT string
	if BOOKS_QUEUE_SERVICE_PORT = os.Getenv("BOOKS_QUEUE_SERVICE_PORT"); BOOKS_QUEUE_SERVICE_PORT == "" {
		BOOKS_QUEUE_SERVICE_PORT = "5672"
	}

	var BOOKS_QUEUE_NAME string
	if BOOKS_QUEUE_NAME = os.Getenv("BOOKS_QUEUE_NAME"); BOOKS_QUEUE_NAME == "" {
		BOOKS_QUEUE_NAME = "books_request"
	}

	log.Println("Connecting to RabbitMQ...")
	conn, err := amqp.Dial("amqp://guest:guest@" + BOOKS_QUEUE_SERVICE_HOST + ":" + BOOKS_QUEUE_SERVICE_PORT + "/")
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	log.Println("Opening channel...")
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	log.Println("Declare queue...")
	q, err := ch.QueueDeclare(
		BOOKS_QUEUE_NAME, // name
		false,            // durable
		false,            // delete when unused
		false,            // exclusive
		false,            // no-wait
		nil,              // arguments
	)
	failOnError(err, "Failed to declare a queue")

	log.Println("Publish message...")
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        reqBody,
		})
	log.Printf(" [x] Sent %s", reqBody)
	failOnError(err, "Failed to publish a message")

	//json.NewEncoder(w).Encode(smt)
	fmt.Fprintf(w, "%s", reqBody)
}

func handleRequests() {
	var PORT string
	if PORT = os.Getenv("PORT"); PORT == "" {
		PORT = "3001"
	}

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/ping", pong)
	myRouter.HandleFunc("/info", info)
	myRouter.HandleFunc("/search", putInQueue).Methods("POST")

	log.Println("Start listen on port " + PORT)
	log.Fatal(http.ListenAndServe(":"+PORT, myRouter))
}

func main() {
	log.Println("Rest API v2.0 - Mux Routers")
	handleRequests()
}
