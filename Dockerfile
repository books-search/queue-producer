# build stage
FROM golang:alpine as BUILD-ENV

ARG BUILD_TIME
ARG COMMIT
ARG RELEASE

ADD . /go/src/

WORKDIR /go/src

RUN apk add --no-cache git && go get github.com/gorilla/mux && go get github.com/streadway/amqp
RUN CGO_ENABLED=0 go build \
	    -ldflags "-X main.version=${RELEASE} -X main.commit=${COMMIT} -X main.buildTime=${BUILD_TIME}" \
		-o ./bin/producer

# build stage
FROM alpine
WORKDIR /app
COPY --from=BUILD-ENV /go/src/bin/producer /app/

ENV PORT=3001
ENV BOOKS_QUEUE_SERVICE_HOST=localhost

ENTRYPOINT ./producer
